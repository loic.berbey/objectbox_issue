import 'package:objectbox/objectbox.dart';

import '../main.dart';

@Entity()
class ShoppingList {
  @Id()
  int id;
  @Unique()
  String uuid;
  @Property(type: PropertyType.date)
  DateTime start;
  @Property(type: PropertyType.date)
  DateTime end;
  @Transient()
  List<String> itemsList = [];

  /// gestion de [itemsList] en base de donnée locale
  List<String> get dbItemsList => itemsList;
  set dbItemsList(List<String> values) {
    itemsList = values;
  }

  @Transient()
  get isDefault => start == DateTime(0, 0, 0) && end == DateTime(0, 0, 0);

  ShoppingList({
    this.id = 0,
    this.uuid = '',
    required this.start,
    required this.end,
    List<String> itemsList = const [],
  }) {
    this.itemsList.addAll(itemsList);
    // generate d'un identifiant pour la base de donnée externe
    if (this.uuid.isEmpty) {
      this.uuid = db.generateUuid();
    }
  }
}
