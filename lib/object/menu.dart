import 'recipeIngredient.dart';

class Menu {
  static const String FIELD_NAME = 'Name';
  static const String FIELD_QUANTITY = 'Quantity';
  static const String FIELD_COVERS_TYPE = 'CoversType';
  static const String FIELD_INGREDIENTS = 'Ingredients';
  static const String FIELD_TIME = 'Time';
  static const String FIELD_TYPE = 'Type';

  int id = 0;
  String name = '';
  int quantity = 0;
  String coversType = '';
  List<RecipeIngredient> ingredientsList = [];
  String time = '';
  String type = '';

  Menu(
      {this.id = 0,
      this.name = '',
      this.quantity = 0,
      this.coversType = '',
      List<RecipeIngredient> ingredientsList = const [],
      this.time = '',
      this.type = ''}) {
    this.ingredientsList.addAll(ingredientsList);
  }

  factory Menu.fromDB(Map<String, dynamic> data) {
    Menu object = Menu(
        name: data[FIELD_NAME],
        quantity: data[FIELD_QUANTITY],
        coversType: data[FIELD_COVERS_TYPE],
        ingredientsList: List.from(data[FIELD_INGREDIENTS])
            .map((value) => RecipeIngredient.fromDB(value))
            .toList(),
        time: data[FIELD_TIME],
        type: data[FIELD_TYPE]);

    return object;
  }

  Map<String, dynamic> toDB() {
    return {
      FIELD_NAME: this.name,
      FIELD_QUANTITY: this.quantity,
      FIELD_COVERS_TYPE: this.coversType,
      FIELD_INGREDIENTS: this
          .ingredientsList
          .map((recipeIngredient) => recipeIngredient.toDB())
          .toList(),
      FIELD_TIME: this.time,
      FIELD_TYPE: this.type,
    };
  }

  @override
  bool operator ==(covariant Menu other) {
    return other.id == id &&
        other.name == name /*&& other.time == time && other.type == type*/;
  }

  int get hashCode => id.hashCode ^ name.hashCode;
}
