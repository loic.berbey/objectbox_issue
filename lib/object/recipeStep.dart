import 'dart:io';

import 'stepTimer.dart';

class RecipeStep {
  static const String FIELD_TEXT = 'Text';
  static const String FIELD_TIMER = 'Timer';
  static const String FIELD_UNIT = 'Unit';

  String text;
  StepTimer timer;
  // TODO PHOTO
  List<File> picturesList = [];

  RecipeStep({this.text = '', this.timer = const StepTimer()});

  factory RecipeStep.fromDB(Map<String, dynamic> data) {
    RecipeStep object = RecipeStep(
        text: data[FIELD_TEXT], timer: StepTimer.fromDB(data[FIELD_TIMER]));
    return object;
  }

  Map<String, dynamic> toDB() {
    return {
      FIELD_TEXT: this.text,
      FIELD_TIMER: this.timer.toDB(),
    };
  }
}
