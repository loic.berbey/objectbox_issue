class StepTimer {
  static const String FIELD_TITLE = 'Title';
  static const String FIELD_DURATION = 'Duration';

  final String title;
  final Duration duration;

  const StepTimer({this.title = '', this.duration = Duration.zero});

  factory StepTimer.fromDB(Map<String, dynamic> data) {
    StepTimer object =
        StepTimer(title: data[FIELD_TITLE], duration: Duration(milliseconds: data[FIELD_DURATION]));
    return object;
  }

  Map<String, dynamic> toDB() {
    return {
      FIELD_TITLE: this.title,
      FIELD_DURATION: this.duration.inMilliseconds,
    };
  }
}
