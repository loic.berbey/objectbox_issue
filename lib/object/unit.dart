import 'package:objectbox/objectbox.dart';

import '../main.dart';

@Entity()
class Unit {
  @Id()
  int id;
  @Unique()
  String uuid;
  @Unique()
  String name;

  Unit({this.id = 0, this.uuid = '', this.name = ''}) {
    // generate d'un identifiant pour la base de donnée externe
    if (this.uuid.isEmpty) {
      this.uuid = db.generateUuid();
    }
  }
}
