import '../main.dart';
import '../objectbox.g.dart';
import 'ingredient.dart';
import 'shop.dart';
import 'unit.dart';

class RecipeIngredient {
  static const String FIELD_INGREDIENT = 'Ingredient';
  static const String FIELD_QUANTITY = 'Quantity';
  static const String FIELD_UNIT = 'Unit';

  Ingredient ingredient;
  double quantity;
  Unit unit;

  RecipeIngredient(
      {required this.ingredient, required this.quantity, required this.unit});

  int get id => ingredient.id;
  String get uuid => ingredient.uuid;
  String get name => ingredient.name;
  String get icon => ingredient.icon;
  Shop get shop => ingredient.shop.target!;

  factory RecipeIngredient.fromDB(Map<String, dynamic> data) {
    RecipeIngredient object = RecipeIngredient(
      ingredient:
          db.getByUuid(db.ingredient, Ingredient_.uuid, data[FIELD_INGREDIENT]),
      quantity: data[FIELD_QUANTITY],
      unit: db.getByUuid(db.unit, Unit_.uuid, data[FIELD_UNIT]),
    );
    return object;
  }

  Map<String, dynamic> toDB() {
    return {
      FIELD_INGREDIENT: this.uuid,
      FIELD_QUANTITY: this.quantity,
      FIELD_UNIT: this.unit.uuid,
    };
  }
}
