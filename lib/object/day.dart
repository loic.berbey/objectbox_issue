import 'dart:convert';

import 'package:objectbox/objectbox.dart';

import '../main.dart';
import 'menu.dart';

@Entity()
class Day {
  @Id()
  int id;
  @Unique()
  String uuid;
  @Property(type: PropertyType.date)
  DateTime date;
  @Transient()
  int get number => date.day;
  @Transient()
  int get placeOverWeek => date.weekday;
  @Transient()
  String get name => '';
  @Transient()
  List<Menu> lunchMenu = [];
  @Transient()
  List<Menu> dinnerMenu = [];

  /// gestion de [lunchMenu] en base de donnée locale
  List<String> get dbLunchMenu =>
      lunchMenu.map((menu) => jsonEncode(menu.toDB())).toList();
  set dbLunchMenu(List<String> values) {
    lunchMenu = values.map((value) => Menu.fromDB(jsonDecode(value))).toList();
  }

  /// gestion de [dinnerMenu] en base de donnée locale
  List<String> get dbDinnerMenu =>
      dinnerMenu.map((menu) => jsonEncode(menu.toDB())).toList();
  set dbDinnerMenu(List<String> values) {
    dinnerMenu = values.map((value) => Menu.fromDB(jsonDecode(value))).toList();
  }

  Day({
    this.id = 0,
    this.uuid = '',
    required this.date,
    List<Menu> lunchMenu = const [],
    List<Menu> dinnerMenu = const [],
  }) {
    this.lunchMenu.addAll(lunchMenu);
    this.dinnerMenu.addAll(dinnerMenu);
    if (this.uuid.isEmpty) {
      this.uuid = db.generateUuid();
    }
  }
}
