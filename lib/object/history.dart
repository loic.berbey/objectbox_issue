import 'package:objectbox/objectbox.dart';

@Entity()
class History {
  @Id()
  int id = 0;
  final String objectName;
  final String uuid;
  @Property(type: PropertyType.date)
  final DateTime dateTime;
  final String operation;

  @Transient()
  String get key => this.objectName + '|' + this.uuid;

  History(
      {this.id = 0,
      required this.objectName,
      required this.uuid,
      required this.dateTime,
      required this.operation});
}
