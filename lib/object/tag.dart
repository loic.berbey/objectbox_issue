import 'package:objectbox/objectbox.dart';

import '../main.dart';

@Entity()
class Tag {
  static const String OBJECT_NAME = 'Tag';
  static const String FIELD_NAME = 'Name';

  @Id()
  int id;
  @Unique()
  String uuid;
  @Unique()
  String name;

  Tag({this.id = 0, this.uuid = '', this.name = ''}) {
    // generate d'un identifiant pour la base de donnée externe
    if (this.uuid.isEmpty) {
      this.uuid = db.generateUuid();
    }
  }
}
