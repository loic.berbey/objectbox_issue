import 'linkedRecipe.dart';
import 'recipeIngredient.dart';
import 'recipeStep.dart';

class Section {
  static const String FIELD_NAME = 'Name';
  static const String FIELD_INGREDIENTS = 'Ingredients';
  static const String FIELD_LINKED_RECIPE = 'LinkedRecipe';
  static const String FIELD_STEPS = 'Steps';

  String name;
  List<RecipeIngredient> ingredientsList = [];
  List<LinkedRecipe> linkedRecipesList = [];
  List<RecipeStep> stepsList = [];

  Section(
      {this.name = '',
      List<RecipeIngredient> ingredientsList = const [],
      List<LinkedRecipe> linkedRecipesList = const [],
      List<RecipeStep> stepsList = const []}) {
    this.ingredientsList.addAll(ingredientsList);
    this.linkedRecipesList.addAll(linkedRecipesList);
    this.stepsList.addAll(stepsList);
  }

  factory Section.fromDB(Map<String, dynamic> data) {
    Section object = Section(
        name: data[FIELD_NAME],
        ingredientsList: List.from(data[FIELD_INGREDIENTS])
            .map<RecipeIngredient>((value) => RecipeIngredient.fromDB(value))
            .toList(),
        linkedRecipesList: List.from(data[FIELD_LINKED_RECIPE])
            .map((value) => LinkedRecipe.fromDB(value))
            .toList(),
        stepsList: List.from(data[FIELD_STEPS])
            .map((value) => RecipeStep.fromDB(value))
            .toList());

    return object;
  }

  Map<String, dynamic> toDB() {
    return {
      FIELD_NAME: this.name,
      FIELD_INGREDIENTS: this
          .ingredientsList
          .map((recipeIngredient) => recipeIngredient.toDB())
          .toList(),
      FIELD_LINKED_RECIPE: this
          .linkedRecipesList
          .map((linkedRecipe) => linkedRecipe.toDB())
          .toList(),
      FIELD_STEPS:
          this.stepsList.map((recipeStep) => recipeStep.toDB()).toList(),
    };
  }
}
