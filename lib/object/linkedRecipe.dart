import '../main.dart';
import '../objectbox.g.dart';
import 'recipe.dart';

class LinkedRecipe {
  static const String FIELD_RECIPE = 'Recipe';
  static const String FIELD_QUANTITY = 'Quantity';

  Recipe recipe;
  int quantity;

  LinkedRecipe({required this.recipe, this.quantity = 0});

  factory LinkedRecipe.fromDB(Map<String, dynamic> data) {
    LinkedRecipe object = LinkedRecipe(
        recipe: db.getByUuid(db.recipe, Recipe_.uuid, data[FIELD_RECIPE]),
        quantity: data[FIELD_QUANTITY]);

    return object;
  }

  Map<String, dynamic> toDB() {
    return {FIELD_RECIPE: this.recipe.uuid, FIELD_QUANTITY: this.quantity};
  }
}
