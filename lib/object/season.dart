class Season {
  final String start;
  final String end;

  const Season({this.start = 'Janvier', this.end = 'Décembre'});

  List<String> toDB() {
    return [this.start, this.end];
  }

  static Season fromDB(List<String> values) {
    return Season(start: values[0], end: values[1]);
  }
}
