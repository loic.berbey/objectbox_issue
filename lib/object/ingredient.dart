import 'package:objectbox/objectbox.dart';

import '../main.dart';
import 'shop.dart';
import 'unit.dart';

@Entity()
class Ingredient {
  @Id()
  int id;
  @Unique()
  String uuid;
  @Unique()
  String name;
  String icon;
  final shop = ToOne<Shop>();
  final units = ToMany<Unit>();

  Ingredient({
    this.id = 0,
    this.uuid = '',
    this.name = '',
    this.icon = '',
    Shop? shop,
    List<Unit> units = const [],
  }) {
    if (shop != null) {
      this.shop.target = shop;
    }
    this.units.addAll(units);
    // generate d'un identifiant pour la base de donnée externe
    if (this.uuid.isEmpty) {
      this.uuid = db.generateUuid();
    }
  }
}
