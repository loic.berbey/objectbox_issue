import 'dart:convert';

import 'package:objectbox/objectbox.dart';

import '../main.dart';
import 'season.dart';
import 'section.dart';
import 'tag.dart';

@Entity()
class Recipe {
  @Id()
  int id;
  @Unique()
  String uuid;
  @Unique()
  String name;
  int defaultQuantity;
  String coversType;
  String difficultyLevel;
  String healthinessLevel;
  @Transient()
  Duration preparationDuration;
  @Transient()
  Duration cookingDuration;
  @Transient()
  Duration breakDuration;
  @Transient()
  Duration get totalDuration => Duration(
      milliseconds: preparationDuration.inMilliseconds +
          cookingDuration.inMilliseconds +
          breakDuration.inMilliseconds);
  bool approvalStatus;
  final tags = ToMany<Tag>();
  List<String> mealTypesList = [];
  @Property(type: PropertyType.date)
  DateTime? lastCookingDate;
  @Property(type: PropertyType.date)
  DateTime? creationDate;
  @Transient()
  Season season;
  @Transient()
  List<Section> sectionsList = [];

  /// gestion de [preparationDuration] en base de donnée locale
  int get dbPreparationDuration => preparationDuration.inMilliseconds;
  set dbPreparationDuration(int value) {
    preparationDuration = Duration(milliseconds: value);
  }

  /// gestion de [cookingDuration] en base de donnée locale
  int get dbCookingDuration => cookingDuration.inMilliseconds;
  set dbCookingDuration(int value) {
    cookingDuration = Duration(milliseconds: value);
  }

  /// gestion de [breakDuration] en base de donnée locale
  int get dbBreakDuration => breakDuration.inMilliseconds;
  set dbBreakDuration(int value) {
    breakDuration = Duration(milliseconds: value);
  }

  /// gestion de [season] en base de donnée locale
  List<String> get dbSeason => season.toDB();
  set dbSeason(List<String> value) {
    season = Season.fromDB(value);
  }

  /// gestion de [sectionsList] en base de donnée locale
  List<String> get dbSectionsList =>
      sectionsList.map((section) => jsonEncode(section.toDB())).toList();
  set dbSectionsList(List<String> values) {
    sectionsList =
        values.map((value) => Section.fromDB(jsonDecode(value))).toList();
  }

  List<String> picturePaths = [];

  Recipe({
    this.id = 0,
    this.uuid = '',
    this.name = '',
    this.defaultQuantity = 0,
    this.coversType = '',
    this.difficultyLevel = '',
    this.healthinessLevel = '',
    this.preparationDuration = Duration.zero,
    this.cookingDuration = Duration.zero,
    this.breakDuration = Duration.zero,
    this.approvalStatus = false,
    List<Tag> tags = const [],
    List<String> mealTypesList = const [],
    this.lastCookingDate,
    this.creationDate,
    this.season = const Season(),
    List<Section> sectionsList = const [],
    List<String> picturePaths = const [],
  }) {
    this.tags.addAll(tags);
    this.mealTypesList.addAll(mealTypesList);
    this.sectionsList.addAll(sectionsList);
    this.picturePaths.addAll(picturePaths);
    // generate d'un identifiant pour la base de donnée externe
    if (this.uuid.isEmpty) {
      this.uuid = db.generateUuid();
    }
  }
}
