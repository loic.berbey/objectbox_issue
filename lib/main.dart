import 'dart:math';

import 'package:flutter/material.dart';
import 'package:objectbox/internal.dart';
import 'package:uuid/uuid.dart';
import 'package:uuid/uuid_util.dart';

import 'object/day.dart';
import 'object/frozenMeal.dart';
import 'object/ingredient.dart';
import 'object/menu.dart';
import 'object/recipe.dart';
import 'object/recipeIngredient.dart';
import 'object/recipeStep.dart';
import 'object/section.dart';
import 'object/shop.dart';
import 'object/stepTimer.dart';
import 'object/tag.dart';
import 'object/unit.dart';
import 'objectbox.g.dart';

late Db db;

class Db {
  Store store;
  late Uuid _uuid;
  Db({required this.store}) {
    _uuid = const Uuid(options: {'grng': UuidUtil.cryptoRNG});
  }

  Box<Shop> get shop => Box<Shop>(store);
  Box<Unit> get unit => Box<Unit>(store);
  Box<Ingredient> get ingredient => Box<Ingredient>(store);
  Box<Tag> get tag => Box<Tag>(store);
  Box<Recipe> get recipe => Box<Recipe>(store);
  Box<FrozenMeal> get frozenMeal => Box<FrozenMeal>(store);
  Box<Day> get day => Box<Day>(store);

  /// Lecture d'un objet ayant pour identifiant externe [uuid]
  getByUuid(Box box, QueryStringProperty uuidFieldName, String uuid) {
    final Query<dynamic> query = box.query(uuidFieldName.equals(uuid)).build();
    dynamic record = query.findFirst();
    query.close();
    return record;
  }

  /// Lecture d'un ou plusieurs objets ayant pour identifiants externe [uuids]
  getManyByUuid(
      Box box, QueryStringProperty uuidFieldName, List<String> uuids) {
    final Query<dynamic> query = box.query(uuidFieldName.oneOf(uuids)).build();
    List<dynamic> records = query.find();
    query.close();
    return records;
  }

  String generateUuid() {
    return _uuid.v4();
  }

  final Random random = Random();
  static const chars = '{!"\/A\$}';
  String generateString(int length) {
    return Iterable.generate(
        length, (idx) => chars[random.nextInt(chars.length)]).join();
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  db = Db(store: await openStore());
  db.shop.removeAll();
  db.unit.removeAll();
  db.ingredient.removeAll();
  db.tag.removeAll();
  db.recipe.removeAll();
  db.day.removeAll();
  db.frozenMeal.removeAll();

  Shop shopA = Shop(name: 'A');
  Shop shopB = Shop(name: 'B');
  db.shop.putMany([shopA, shopB]);

  Unit unitA = Unit(name: 'A');
  Unit unitB = Unit(name: 'B');
  db.unit.putMany([unitA, unitB]);

  Ingredient ingA =
      Ingredient(name: 'A', icon: 'A', shop: shopB, units: [unitB]);
  Ingredient ingB =
      Ingredient(name: 'B', icon: 'B', shop: shopA, units: [unitA, unitB]);
  db.ingredient.putMany([ingA, ingB]);

  Recipe recipeA = Recipe(name: 'Recipe A', sectionsList: [
    Section(name: 'DEFAULT_SECTION'),
    Section(name: 'Section 1', ingredientsList: [
      RecipeIngredient(ingredient: ingA, quantity: 1, unit: unitB),
      RecipeIngredient(ingredient: ingB, quantity: 2, unit: unitA)
    ], stepsList: [
      RecipeStep(text: 'Step 1.1'),
      RecipeStep(
          text: 'Step 1.2',
          timer: StepTimer(duration: Duration(minutes: 10), title: 'Step 1.2'))
    ]),
    Section(name: 'Section 2', ingredientsList: [
      RecipeIngredient(ingredient: ingA, quantity: 3, unit: unitB),
      RecipeIngredient(ingredient: ingB, quantity: 4, unit: unitA)
    ], stepsList: [
      RecipeStep(text: 'Step 2.1'),
      RecipeStep(
          text: 'Step 2.2',
          timer: StepTimer(duration: Duration(minutes: 10), title: 'Step 2.2'))
    ])
  ], picturePaths: [
    db.generateString(300),
    db.generateString(700)
  ]);
  Recipe recipeB = Recipe(name: 'Recipe B', sectionsList: [
    Section(name: 'DEFAULT_SECTION', ingredientsList: [
      RecipeIngredient(ingredient: ingB, quantity: 3, unit: unitB)
    ], stepsList: [
      RecipeStep(text: 'Step 1'),
      RecipeStep(
          text: 'Setp 2',
          timer: StepTimer(duration: Duration(minutes: 60), title: 'Step 2'))
    ])
  ], picturePaths: [
    db.generateString(300),
    db.generateString(700)
  ]);
  db.recipe.putMany([recipeA, recipeB]);

  FrozenMeal frozenA = FrozenMeal(name: 'A', quantity: 2, coversType: 'A');
  FrozenMeal frozenB = FrozenMeal(name: 'B', quantity: 10, coversType: 'B');
  db.frozenMeal.putMany([frozenA, frozenB]);

  Day currentDay = Day(
      date: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day),
      lunchMenu: [
        Menu(
            name: 'A',
            type: 'A',
            time: 'A',
            quantity: 10,
            coversType: 'A',
            ingredientsList: [
              RecipeIngredient(ingredient: ingB, quantity: 3, unit: unitB)
            ])
      ],
      dinnerMenu: [
        Menu(
            id: frozenA.id,
            name: 'B',
            type: 'B',
            time: 'B',
            quantity: 2,
            coversType: 'B'),
        Menu(
            id: recipeB.id,
            name: 'C',
            type: 'C',
            time: 'C',
            quantity: 2,
            coversType: 'C',
            ingredientsList: [
              ...recipeB.sectionsList
                  .expand((section) => section.ingredientsList)
                  .toList(),
              ...recipeB.sectionsList
                  .expand((section) => section.linkedRecipesList.expand(
                      (linkedRecipe) => linkedRecipe.recipe.sectionsList
                          .expand((section) => section.ingredientsList)
                          .toList()))
                  .toList()
            ])
      ]);
  db.day.put(currentDay);

  List<Shop> shops = db.shop.getAll();
  // Size must be two
  shops = db.getManyByUuid(
      db.shop, Shop_.uuid, shops.map((shop) => shop.uuid).toList());
  // Size must be two

  List<Recipe> recipes = db.recipe.getAll();
  // Size must be two
  recipes = db.getManyByUuid(
      db.recipe, Recipe_.uuid, recipes.map((recipe) => recipe.uuid).toList());
  // Size must be two but it's one

  List<Day> days = db.day.getAll();
  // lunchMenu and dinnerMenu must be filled

  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: Center(
          child: Text('Hello World!'),
        ),
      ),
    );
  }
}
