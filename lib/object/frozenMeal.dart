import 'package:objectbox/objectbox.dart';

import '../main.dart';

@Entity()
class FrozenMeal {
  @Id()
  int id;
  @Unique()
  String uuid;
  @Unique()
  String name;
  @Property(type: PropertyType.date)
  late DateTime freezingDate;
  int quantity;
  String coversType;

  FrozenMeal(
      {this.id = 0,
      this.uuid = '',
      this.name = '',
      DateTime? freezingDate,
      this.quantity = 0,
      this.coversType = ''}) {
    this.freezingDate = freezingDate ?? DateTime.now();
    // generate d'un identifiant pour la base de donnée externe
    if (this.uuid.isEmpty) {
      this.uuid = db.generateUuid();
    }
  }
}
